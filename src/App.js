import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
  };

  addAndClear = event => {
    if (event.key === "Enter") {
      const todoInput = {
        "userId": 1,
        "id": Math.floor(Math.random()*100),
        "title": event.target.value,
        "completed": false
      }
      let newTodo = this.state.todos.slice()
      newTodo.push(todoInput)
      this.setState({todos: newTodo})
      event.target.value = null
      console.log("It Works!")
    }
  }

  deleteTodo = (event, todoId) => {
    let removeTodo = this.state.todos.slice()
    let editedTodo = removeTodo.filter(todo => todo.id !== todoId)
    this.setState({todos: editedTodo})
    console.log("Deleted")
  }

  markComplete = (event, todoId) => {
    let completeTodo = this.state.todos.slice()
    let completedTodo = completeTodo.map(todo => {
      if (todo.id === todoId) {
        todo.completed = !todo.completed
        console.log(todo.completed)
      }
      return todo
    })
    this.setState({todos: completedTodo})
    console.log("Checked it!")
  }

  clearComplete = (event) => {
    let completeTodo = 
      this.state.todos.filter(todo => todo.completed === false)
    this.setState({todos: completeTodo})
  }


  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input 
            className="new-todo" 
            placeholder="What needs to be done?" 
            onKeyDown={this.addAndClear} 
            autoFocus 
          />
        </header>
        <TodoList 
          todos={this.state.todos}
          delete={this.deleteTodo}
          complete={this.markComplete} />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button 
            className="clear-completed" 
            onClick={this.clearComplete}
          >
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input 
            className="toggle" 
            type="checkbox" 
            checked={this.props.completed}
            onChange=
              {(event) => this.props.complete(event, this.props.id)}
          />
          <label>{this.props.title}</label>
          <button 
            className="destroy"
            onClick=
              {(event) => this.props.delete(event, this.props.id)}
          />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem 
              id={todo.id}
              title={todo.title}
              completed={todo.completed} 
              complete={this.props.complete}
              delete={this.props.delete} 
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
